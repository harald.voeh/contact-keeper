#!/bin/sh
docker-compose exec backend python -m pytest "project/tests"
docker-compose exec backend flake8 project
docker-compose exec backend black project --check
docker-compose exec backend /bin/sh -c "isort project/**/*.py"
# run checks for react
docker-compose exec client npm run prettier:check
docker-compose exec client npm run lint