import http from "./httpService";
import jwtDecode from "jwt-decode";

const API = process.env.REACT_APP_USERS_SERVICE_URL;
const tokenKey = "authToken";

http.setJwt(getJwt());

export function register(user) {
  return http.post(`${API}/auth/register`, {
    username: user.username,
    email: user.email,
    password: user.password
  });
}

export async function login(email, password) {
  const { data: jwt } = await http.post(`${API}/auth/login`, {
    email,
    password
  });
  localStorage.setItem(tokenKey, jwt.auth_token);
}

export function logout() {
  localStorage.removeItem(tokenKey);
}

export function refresh(token) {
  return http.post(`${API}/auth/refresh`, { refresh_token: token });
}

export function getJwt() {
  return localStorage.getItem(tokenKey);
}

export function getCurrentUser() {
  try {
    const token = localStorage.getItem(tokenKey);
    return jwtDecode(token);
  } catch (ex) {
    return null;
  }
}

export function status() {
  return http.get(`${API}/auth/status`);
}

export function contacts() {
  return http.get(`${API}/contact`);
}

export default {
  login,
  logout,
  register,
  status,
  getCurrentUser,
  getJwt,
  contacts
};
