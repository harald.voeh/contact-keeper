import http from "./httpService";

const API = process.env.REACT_APP_USERS_SERVICE_URL;

export function getUsers() {
  return http.get(`${API}/user`);
}

export function addUser(user) {
  return http.post(`${API}/user`, user);
}
