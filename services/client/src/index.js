import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import ContactState from "./context/contact/ContactState";
import AuthState from "./context/auth/AuthState";
import AlertState from "./context/alert/AlertState";
import App from "./App";

// import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.css";

ReactDOM.render(
  <AuthState>
    <ContactState>
      <AlertState>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </AlertState>
    </ContactState>
  </AuthState>,
  document.getElementById("root")
);
