import React, { useContext } from "react";
import PropTypes from "prop-types";
import ContactContext from "../../context/contact/contactContext";

const ContactItem = ({ contact }) => {
  const contactContext = useContext(ContactContext);
  const { id, name, email, phone, type } = contact;
  const handleDelete = () => {
    contactContext.deleteContact(id);
    contactContext.clearCurrent();
  };
  return (
    <div className="card bg-light">
      <h3 className="text-primary text-left">
        {name}{" "}
        <span
          style={{ float: "right" }}
          className={
            "badge badge-" + (type === "professional" ? "success" : "primary")
          }
        >
          {type.charAt(0).toUpperCase() + type.slice(1)}
        </span>
        <ul className="list">
          {email && (
            <li>
              <i className="fa fa-envelope-open" /> {email}
            </li>
          )}
          {phone && (
            <li>
              <i className="fa fa-phone" /> {phone}
            </li>
          )}
        </ul>
        <button
          className="btn btn-dark btn-sm"
          onClick={() => contactContext.setCurrent(contact)}
        >
          Edit
        </button>
        <button className="btn btn-danger btn-sm" onClick={handleDelete}>
          Delete
        </button>
      </h3>
    </div>
  );
};

ContactItem.propTypes = {
  contact: PropTypes.object.isRequired
};

export default ContactItem;
