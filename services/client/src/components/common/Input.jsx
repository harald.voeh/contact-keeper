import React from "react";

const Input = ({ name, label, caption, error, ...rest }) => {
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <input id={name} name={name} {...rest} className="form-control" />
      {caption && <small className="form-text text-muted">{caption}</small>}
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Input;
