import React, { useReducer } from "react";
import ContactContext from "./contactContext";
import contactReducer from "./contactReducer";
import axios from "axios";

import {
  ADD_CONTACT,
  DELETE_CONTACT,
  SET_CURRENT,
  CLEAR_CURRENT,
  UPDATE_CONTACT,
  FILTER_CONTACTS,
  CLEAR_FILTER,
  CONTACT_ERROR,
  GET_CONTACTS,
  CLEAR_CONTACTS
} from "../types";

const API = process.env.REACT_APP_USERS_SERVICE_URL;

const ContactState = props => {
  const initialState = {
    contacts: null,
    current: null,
    filtered: null,
    loading: true,
    error: null
  };

  const [state, dispatch] = useReducer(contactReducer, initialState);

  const getContacts = async () => {
    try {
      const { data } = await axios.get(`${API}/contact`);
      dispatch({ type: GET_CONTACTS, payload: data });
    } catch (err) {
      dispatch({ type: CONTACT_ERROR, payload: err.response });
    }
  };
  // add contact
  const addContact = async contact => {
    try {
      const { data } = await axios.post(`${API}/contact`, contact);
      dispatch({ type: ADD_CONTACT, payload: data });
    } catch (err) {
      dispatch({ type: CONTACT_ERROR, payload: err.response });
    }
  };
  // delete contact
  const deleteContact = async id => {
    try {
      const { data } = axios.delete(`${API}/contact/${id}`);
      dispatch({ type: DELETE_CONTACT, payload: id });
    } catch (err) {
      dispatch({ type: CONTACT_ERROR, payload: err.response });
    }
  };
  // set current contact
  const setCurrent = contact => {
    dispatch({ type: SET_CURRENT, payload: contact });
  };
  // clear current contact
  const clearCurrent = () => {
    dispatch({ type: CLEAR_CURRENT });
  };
  // update contact
  const updateContact = async contact => {
    try {
      const { data } = await axios.put(`${API}/contact/${contact.id}`, contact);
      dispatch({ type: UPDATE_CONTACT, payload: data });
    } catch (err) {
      dispatch({ type: CONTACT_ERROR, payload: err.response });
    }
    dispatch({ type: UPDATE_CONTACT, payload: contact });
  };
  // filter contact
  const filterContacts = text => {
    console.log(text);
    dispatch({ type: FILTER_CONTACTS, payload: text });
  };
  // clear filter
  const clearFilter = () => {
    dispatch({ type: CLEAR_FILTER });
  };
  const clearContacts = () => {
    dispatch({ type: CLEAR_CONTACTS });
  };

  return (
    <ContactContext.Provider
      value={{
        contacts: state.contacts,
        current: state.current,
        filtered: state.filtered,
        error: state.error,
        loading: state.loading,
        addContact,
        deleteContact,
        setCurrent,
        clearCurrent,
        updateContact,
        filterContacts,
        clearFilter,
        getContacts,
        clearContacts
      }}
    >
      {props.children}
    </ContactContext.Provider>
  );
};

export default ContactState;
