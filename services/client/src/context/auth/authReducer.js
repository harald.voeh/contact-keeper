import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from "../types";

export default (state, action) => {
  switch (action.type) {
    case USER_LOADED:
      return {
        ...state,
        isAuthenticated: true,
        loading: false,
        user: action.payload
      };
    case LOGIN_SUCCESS:
      localStorage.setItem("token", action.payload.auth_token);
      return {
        ...state,
        response: {
          status: 200,
          message: "Login successful."
        },
        loading: false,
        isAuthenticated: true
      };

    case REGISTER_SUCCESS:
      return {
        ...state,
        response: action.payload,
        loading: false
      };

    case REGISTER_FAIL:
    case AUTH_ERROR:
    case LOGIN_FAIL:
    case LOGOUT:
      localStorage.removeItem("token");
      return {
        ...state,
        token: null,
        isAuthenticated: false,
        loading: false,
        user: null,
        response: action.payload
      };
    case CLEAR_ERRORS:
      return { ...state, response: null };
    default:
      return state;
  }
};
