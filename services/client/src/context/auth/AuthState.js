import React, { useReducer } from "react";
import AuthContext from "./authContext";
import authReducer from "./authReducer";
import axios from "axios";

import setAuthToken from "../../utils/setAuthToken";

import {
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  USER_LOADED,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT,
  CLEAR_ERRORS
} from "../types";

const API = process.env.REACT_APP_USERS_SERVICE_URL;

const AuthState = props => {
  const initialState = {
    token: localStorage.getItem("token"),
    isAuthenticated: null,
    user: null,
    loading: true,
    error: null,
    response: null
  };

  const [state, dispatch] = useReducer(authReducer, initialState);
  // load user
  const loadUser = async () => {
    // @todo
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }

    try {
      const { data } = await axios.get(`${API}/auth/status`);
      console.log(data);
      dispatch({ type: USER_LOADED, payload: data });
    } catch (err) {
      dispatch({ type: AUTH_ERROR });
    }
  };
  // register user
  const register = async formData => {
    try {
      const res = await axios.post(`${API}/auth/register`, formData);
      console.log(res);
      console.log(res.data);
      dispatch({
        type: REGISTER_SUCCESS,
        payload: {
          status: res.status,
          message: "User successfully registered. You can login now."
        }
      });
    } catch (err) {
      console.log(err.response);
      dispatch({
        type: REGISTER_FAIL,
        payload: {
          status: err.response.status,
          message: err.response.data.message
        }
      });
    }
  };
  // login user
  const login = async formData => {
    console.log(formData);

    try {
      const { data } = await axios.post(`${API}/auth/login`, formData);
      console.log(data);
      //console.log(res.data);
      dispatch({
        type: LOGIN_SUCCESS,
        payload: await data
      });
      loadUser();
    } catch (err) {
      console.log(err.response);
      dispatch({
        type: LOGIN_FAIL,
        payload: {
          status: err.response.status,
          message: err.response.data.message
        }
      });
    }
  };
  // logout user
  const logout = () => {
    setAuthToken();
    dispatch({ type: LOGOUT });
  };
  // clear errors
  const clearErrors = () => {
    dispatch({ type: CLEAR_ERRORS });
  };
  //

  return (
    <AuthContext.Provider
      value={{
        token: state.token,
        isAuthenticated: state.isAuthenticated,
        loading: state.loading,
        user: state.user,
        error: state.error,
        response: state.response,
        loadUser,
        register,
        login,
        logout,
        clearErrors
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
