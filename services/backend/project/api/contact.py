import jwt
from flask import request
from flask_restx import Namespace, Resource, fields

from project.api.crud import add_contact, delete_contact, get_user_by_id, update_contact
from project.api.models import Contact, User

contact_namespace = Namespace("contact")

contact = contact_namespace.model(
    "Contact",
    {
        "id": fields.Integer(readOnly=True),
        "name": fields.String(required=True),
        "email": fields.String(required=True),
        "phone": fields.String(required=True),
        "type": fields.String(required=True),
    },
)


parser = contact_namespace.parser()
parser.add_argument("Authorization", location="headers")


def authenticate(auth_header=None):
    auth_header = request.headers.get("Authorization")
    if auth_header:
        try:
            access_token = auth_header.split(" ")[1]
            resp = User.decode_token(access_token)
            user = get_user_by_id(resp)
            if not user:
                contact_namespace.abort(401, "Invalid token")
            return user, 200
        except jwt.ExpiredSignatureError:
            contact_namespace.abort(401, "Signature expired. Please log in again.")
        except jwt.InvalidTokenError:
            contact_namespace.abort(401, "Invalid token. Please log in again.")
    else:
        contact_namespace.abort(403, "Token required")


class ContactList(Resource):
    @contact_namespace.marshal_with(contact, as_list=True)
    @contact_namespace.response(200, "Success")
    @contact_namespace.response(401, "Invalid token")
    @contact_namespace.expect(parser)
    def get(self):
        user, response = authenticate()
        return user.contacts, response

    @contact_namespace.marshal_with(contact)
    @contact_namespace.response(200, "Success")
    @contact_namespace.response(401, "Invalid token")
    @contact_namespace.expect(parser)
    def post(self):
        user, response = authenticate()

        post_data = request.get_json()
        name = post_data.get("name")
        email = post_data.get("email")
        phone = post_data.get("phone")
        type = post_data.get("type")
        contact = Contact(name=name, email=email, phone=phone, type=type)
        add_contact(user, contact)
        return contact, 200


class Contacts(Resource):
    @contact_namespace.marshal_with(contact)
    @contact_namespace.response(200, "Success")
    @contact_namespace.response(401, "Invalid token")
    @contact_namespace.response(404, "Contact does not exist")
    @contact_namespace.response(404, "Contact does not exist")
    @contact_namespace.expect(parser)
    def delete(self, contact_id):
        user, response = authenticate()
        contact = Contact.query.filter_by(id=contact_id).first()

        if contact is None:
            contact_namespace.abort(404, f"Contact {contact_id} does not exist")

        if contact not in user.contacts:
            contact_namespace.abort(403, "This contact belongs to another user.")

        contact = delete_contact(user, contact)

        return contact, 200

    @contact_namespace.marshal_with(contact)
    @contact_namespace.response(200, "Success")
    @contact_namespace.response(401, "Invalid token")
    @contact_namespace.response(404, "Contact does not exist")
    @contact_namespace.response(404, "Contact does not exist")
    @contact_namespace.expect(parser)
    def put(self, contact_id):
        user, response = authenticate()
        contact = Contact.query.filter_by(id=contact_id).first()
        print("dsgsfg")

        if contact is None:
            contact_namespace.abort(404, f"Contact {contact_id} does not exist")

        if contact not in user.contacts:
            contact_namespace.abort(403, "This contact belongs to another user.")

        post_data = request.get_json()
        contact = update_contact(contact, post_data)

        return contact, 200


contact_namespace.add_resource(ContactList, "")
contact_namespace.add_resource(Contacts, "/<int:contact_id>")
