from flask_restx import Api

from project.api.auth import auth_namespace
from project.api.contact import contact_namespace
from project.api.ping import ping_namespace
from project.api.users import users_namespace

api = Api(version="1.0", title="API", doc="/doc/")
api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(users_namespace, path="/user")
api.add_namespace(auth_namespace, path="/auth")
api.add_namespace(contact_namespace, path="/contact")
