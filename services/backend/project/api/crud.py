from project import db
from project.api.models import User


def get_all_users():
    return User.query.all()


def get_user_by_id(user_id):
    return User.query.filter_by(id=user_id).first()


def get_user_by_email(email):
    return User.query.filter_by(email=email).first()


def add_user(username, email, password):
    user = User(username=username, email=email, password=password)
    db.session.add(user)
    db.session.commit()
    return user


def update_user(user, username, email):
    user.username = username
    user.email = email
    db.session.commit()
    return user


def delete_user(user):
    db.session.delete(user)
    db.session.commit()
    return user


def add_contact(user, contact):
    user.contacts.append(contact)
    db.session.commit()
    return user


def delete_contact(user, contact):
    user.contacts.remove(contact)
    # db.session.delete(contact)
    db.session.commit()
    return contact


def update_contact(contact, post_data):
    if post_data.get("name"):
        contact.name = post_data["name"]
    if post_data.get("email"):
        contact.email = post_data["name"]
    if post_data.get("phone"):
        contact.phone = post_data["email"]
    if post_data.get("type"):
        contact.type = post_data["type"]
    db.session.commit()
    return contact
