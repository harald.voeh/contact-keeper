import json

CONTACT1 = {
    "name": "angie",
    "email": "angie@bundestag.de",
    "phone": "666666",
    "type": "personal",
}

CONTACT2 = {
    "name": "gabriel",
    "email": "gabbie@bundestag.de",
    "phone": "222222",
    "type": "professional",
}


def test_add_contact(test_app, test_database, add_user):
    add_user("test6", "test6@test.com", "test")
    client = test_app.test_client()
    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test6@test.com", "password": "test"}),
        content_type="application/json",
    )
    token = json.loads(resp_login.data.decode())["auth_token"]
    resp = client.post(
        "/contact",
        data=json.dumps(CONTACT1),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert "angie" in data["name"]
    assert "angie@bundestag.de" in data["email"]
    assert "666666" in data["phone"]
    assert "personal" in data["type"]


def test_add_unauthorised_contact(test_app, test_database, add_user):
    add_user("test6", "test6@test.com", "test")

    client = test_app.test_client()
    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test6@test.com", "password": "test"}),
        content_type="application/json",
    )
    token = json.loads(resp_login.data.decode())["auth_token"]
    resp = client.post(
        "/contact",
        data=json.dumps(CONTACT1),
        headers={"Authorization": f"Bearer {token}sde"},
        content_type="application/json",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token. Please log in again." in data["message"]


def test_get_contact_list(test_app, test_database, add_user, add_contact):
    user = add_user("test8", "test10@test.com", "test")
    add_contact(user, **CONTACT2)
    add_contact(user, **CONTACT1)
    client = test_app.test_client()
    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test10@test.com", "password": "test"}),
        content_type="application/json",
    )
    token = json.loads(resp_login.data.decode())["auth_token"]

    resp = client.get(
        "/contact",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert 2 == len(data)
    assert "angie" in data[1]["name"]
    assert "angie@bundestag.de" in data[1]["email"]
    assert "666666" in data[1]["phone"]
    assert "personal" in data[1]["type"]


def test_get_unauthorised_contact_list(test_app, test_database, add_user, add_contact):
    user = add_user("test8", "test10@test.com", "test")
    add_contact(user, **CONTACT2)
    add_contact(user, **CONTACT1)
    client = test_app.test_client()
    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test10@test.com", "password": "test"}),
        content_type="application/json",
    )
    token = json.loads(resp_login.data.decode())["auth_token"]

    resp = client.get(
        "/contact",
        headers={"Authorization": f"Bearer {token}3"},
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert resp.content_type == "application/json"
    assert "Invalid token. Please log in again." in data["message"]


def test_delete_contact(test_app, test_database, add_user, add_contact):
    user = add_user("test8", "test12@test.com", "test")
    contact_1 = add_contact(user, **CONTACT1)
    add_contact(user, **CONTACT2)
    client = test_app.test_client()

    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test12@test.com", "password": "test"}),
        content_type="application/json",
    )

    token = json.loads(resp_login.data.decode())["auth_token"]

    # Test if two users are listed
    resp = client.get(
        "/contact",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )

    # Delete user
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert 2 == len(data)

    resp = client.delete(
        f"/contact/{contact_1.id}",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert "angie" in data["name"]

    # Test if one users is listed
    resp = client.get(
        "/contact",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert 1 == len(data)


def test_delete_other_contact(test_app, test_database, add_user, add_contact):
    user_1 = add_user("test8", "test34@test.com", "test")
    user_2 = add_user("test8", "test77@test.com", "test")
    add_contact(user_1, **CONTACT1)
    contact_2 = add_contact(user_2, **CONTACT2)
    client = test_app.test_client()

    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test34@test.com", "password": "test"}),
        content_type="application/json",
    )

    token = json.loads(resp_login.data.decode())["auth_token"]

    resp = client.delete(
        f"/contact/{contact_2.id}",
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert resp.content_type == "application/json"
    assert "This contact belongs to another user." in data["message"]


def test_put_contact(test_app, test_database, add_user, add_contact):
    user_1 = add_user("test8", "test39@test.com", "test")
    contact_1 = add_contact(user_1, **CONTACT1)
    client = test_app.test_client()

    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": "test39@test.com", "password": "test"}),
        content_type="application/json",
    )

    token = json.loads(resp_login.data.decode())["auth_token"]

    resp = client.put(
        f"/contact/{contact_1.id}",
        data=json.dumps({"name": "Fridolin"}),
        headers={"Authorization": f"Bearer {token}"},
        content_type="application/json",
    )

    data = json.loads(resp.data.decode())

    assert resp.status_code == 200
    assert resp.content_type == "application/json"
    assert "Fridolin" in data["name"]
    assert "angie@bundestag.de" in data["email"]
    assert "666666" in data["phone"]
    assert "personal" in data["type"]
