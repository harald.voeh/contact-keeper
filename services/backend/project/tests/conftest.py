import pytest

from project import create_app, db  # updated
from project.api.models import Contact, User


@pytest.fixture(scope="module")
def test_app():
    app = create_app()  # new
    app.config.from_object("project.config.TestingConfig")
    with app.app_context():
        yield app  # testing happens here


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    yield db  # testing happens here
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="module")
def add_user():
    def _add_user(username, email, password):
        user = User(username=username, email=email, password=password)
        db.session.add(user)
        db.session.commit()
        return user

    return _add_user


@pytest.fixture(scope="module")
def add_contact():
    def _add_contact(user, **kwargs):
        contact = Contact(**kwargs)
        user.contacts.append(contact)
        db.session.add(contact)
        db.session.commit()
        return contact

    return _add_contact
