# Contact Keeper app

This is a simple web application to store contacts. The site allows users to register and enables them to save contacts after the login. The website runs a flask REST-API and react as a front end.

The application can be previewed: https://baroque-chaise-95877.herokuapp.com (may takes some time to load)

## Screenshot

![UI](UI.png)
